﻿using System;

namespace PhonebookLib
{
    public class PhonebookEntry
    {
        #region Properties

        /// <summary>
        /// Entry ID.
        /// </summary>
        public int Id { get; set; }


        /// <summary>
        /// Person's first name.
        /// </summary>
        public string FirstName { get; set; }


        /// <summary>
        /// Person's last name.
        /// </summary>
        public string LastName { get; set; }


        /// <summary>
        /// Year of birth.
        /// </summary>
        public int BirthYear { get; set; }

        #endregion


        #region API

        public bool IsMatch(string filter)
        {
            if (string.IsNullOrEmpty(filter)) return true;

            return FirstName.IndexOf(filter, StringComparison.CurrentCultureIgnoreCase) >= 0 ||
                   LastName.IndexOf(filter, StringComparison.CurrentCultureIgnoreCase) >= 0 ||
                   BirthYear.ToString().Contains(filter);
        }

        #endregion
    }
}