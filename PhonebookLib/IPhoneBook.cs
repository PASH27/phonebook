﻿using System.Collections.ObjectModel;


namespace PhonebookLib
{
    /// <summary>
    /// Generalized functionality for storing and retrieving phonebook entries.
    /// </summary>
    public interface IPhonebook
    {
        /// <summary>
        /// Get all entries as a list.
        /// </summary>
        ReadOnlyCollection<PhonebookEntry> GetList(string filter = "");


        /// <summary>
        /// Add a new entry/
        /// </summary>
        void Add(PhonebookEntry entry);


        /// <summary>
        /// Delete an existing entry by its ID.
        /// </summary>
        void Delete(int id);
    }
}