﻿using System;
using System.Data.SqlClient;


namespace PhonebookLib
{
    public class SqlSettings
    {
        #region Constants

        public const string DefaultConnection = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=Phonebook;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

        #endregion


        #region Properties

        public Type ConnectionType { get; set; } = typeof(SqlConnection);


        public string ConnectionString { get; set; } = DefaultConnection;

        #endregion
    }
}
