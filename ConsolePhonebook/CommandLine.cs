﻿namespace ConsoleApp
{
    public static class CommandLine
    {
        public const string Help = "help";

        public const string List = "list";

        public const string Add = "add";

        public const string Delete = "del";

        public const string TryHelpText = "Unknown argument. Try 'help'.";

        public static string HelpText =>
            $@"PhoneBook arguments:
{List} [filter] - show entries (with optional filter)
{Add} - add a new entry (data from stdin)
{Delete} - delete an entry by ID (from stdin)
{Help} - this text";
    }
}
